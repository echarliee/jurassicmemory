package com.example.jurassicmemory

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.jurassicmemory.databinding.ActivityGameBinding

class GameActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGameBinding
    private var mov = 0
    private lateinit var buttons: MutableList<ImageButton?>
    private lateinit var images: MutableList<Int>
    private var shown: MutableList<Int> = arrayListOf()

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState.putInt("mov", mov)
        savedInstanceState.putIntegerArrayList("images", ArrayList(images))
        savedInstanceState.putIntegerArrayList("shown", ArrayList(shown))
    }

    override  fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_JurassicMemory)
        super.onCreate(savedInstanceState)

        binding = ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle: Bundle? = intent.extras
        val difficulty = bundle?.getString("difficulty")

        // moviments
        if (savedInstanceState != null) mov = savedInstanceState.getInt("mov")
        binding.movements.setText(getString(R.string.movements, mov))

        images = mutableListOf(
            R.raw.dilophosaurus,
            R.raw.dilophosaurus,
            R.raw.trex,
            R.raw.trex,
            R.raw.triceratops,
            R.raw.triceratops,
            R.raw.velociraptor,
            R.raw.velociraptor
        )
        buttons = mutableListOf(
            binding.button1, binding.button2, binding.button3, binding.button4,
            binding.button5, binding.button6, binding.button7, binding.button8
        )

        // si la dificultat és fàcil, treiem elements de les llistes i del xml
        if(difficulty == "easy"){
            images.removeLast()
            images.removeLast()
            buttons.removeLast()
            buttons.removeLast()
            binding.button7!!.visibility = View.GONE
            binding.button8!!.visibility = View.GONE
        }

        images.shuffle()

        // si tenim coses guardades les posem
        if (savedInstanceState != null){
            images = savedInstanceState.getIntegerArrayList("images")!!
        }

        var clicked = 0
        var lastClicked = -1

        val size = buttons.size - 1
        for (i in 0..size) {
            buttons[i]!!.contentDescription = "cardBack"
            // si tenim cartes destapades les mostrem
            if(savedInstanceState != null){
                val shownAux = savedInstanceState.getIntegerArrayList("shown")
                if(shownAux!!.contains(i)){
                    buttons[i]!!.setBackgroundResource(images[i])
                    buttons[i]!!.contentDescription = images[i].toString()
                    shown.add(i)
                    shown.add(lastClicked)
                }
            }
            buttons[i]!!.setOnClickListener {
                if (clicked < 2 && buttons[i]!!.contentDescription == "cardBack") {
                    buttons[i]!!.setBackgroundResource(images[i])
                    buttons[i]!!.contentDescription = images[i].toString()
                    if (clicked == 0) {
                        lastClicked = i
                    }
                    clicked++
                }
                // quan hem destapat dues cartes comprobem si son iguals
                if (clicked == 2) {
                    clicked++
                    // sempre actualitzem els moviments
                    mov++
                    binding.movements.setText(getString(R.string.movements, mov))
                    if (buttons[i]!!.contentDescription == buttons[lastClicked]!!.contentDescription) {
                        // si són iguals les deixem així i ja no són clicables
                        buttons[i]!!.isClickable = false
                        buttons[lastClicked]!!.isClickable = false
                        clicked = 0
                        // les guardem a l'array de mostrades
                        shown.add(i)
                        shown.add(lastClicked)
                        // si s'han destapat totes les cartes passem de pantalla
                        if (isFinished(buttons)){
                            val intent = Intent(this, ScoreActivity::class.java)
                            intent.putExtra("difficulty", difficulty)
                            intent.putExtra("movements", mov)
                            startActivity(intent)
                        }
                    } else {
                        // si són diferents tornem a posar el back
                        Handler(Looper.getMainLooper()).postDelayed({
                            buttons[i]!!.contentDescription = "cardBack"
                            buttons[i]!!.setBackgroundResource(R.drawable.back)
                            buttons[lastClicked]!!.contentDescription = "cardBack"
                            buttons[lastClicked]!!.setBackgroundResource(R.drawable.back)
                            clicked = 0
                        }, 2000)
                    }
                }
            }
        }
    }

    private fun isFinished(buttons: MutableList<ImageButton?>): Boolean{
        val size = buttons.size - 1
        for (i in 0..size){
            if(buttons[i]!!.isVisible && buttons[i]!!.contentDescription == "cardBack") return false
        }
        return true
    }
}