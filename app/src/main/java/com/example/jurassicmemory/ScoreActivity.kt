package com.example.jurassicmemory

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.jurassicmemory.databinding.ActivityScoreBinding

class ScoreActivity : AppCompatActivity() {
    private lateinit var binding: ActivityScoreBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_JurassicMemory)
        super.onCreate(savedInstanceState)

        binding = ActivityScoreBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle: Bundle? = intent.extras
        val difficulty = bundle?.getString("difficulty")
        val mov = bundle?.getInt("movements")

        // posar puntuació
        var minMov = 4
        if(difficulty == "easy") minMov = 3
        val score = mov?.let { (minMov.times(100))?.div(it) }
        binding.score.text = score.toString()

        // tornar a l'inici
        binding.buttonStart.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        // tornar a jugar
        binding.buttonRepeat.setOnClickListener {
            val intent = Intent(this, GameActivity::class.java)
            intent.putExtra("difficulty", difficulty)
            startActivity(intent)
        }

    }

}