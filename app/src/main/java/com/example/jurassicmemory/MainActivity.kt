package com.example.jurassicmemory

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.jurassicmemory.databinding.ActivityMainBinding
import java.lang.Thread.sleep

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    /*
    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState.putString("HELP_TEXT", binding.buttonHelp.text.toString())
    }
    */
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_JurassicMemory)
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /*
        if (savedInstanceState != null) {
            binding.buttonHelp.text = savedInstanceState.getString("HELP_TEXT")
        }
        */

        // when button is clicked, show the alert
        val btnShowAlert = binding.buttonHelp
        btnShowAlert.setOnClickListener {
            // build alert dialog
            val dialogBuilder = AlertDialog.Builder(this)
            // set message of alert dialog
            dialogBuilder.setMessage(R.string.game_instructions_txt)
                // negative button text and action
                .setNegativeButton(R.string.close, DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                })
            // create dialog box
            val alert = dialogBuilder.create()
            // set title for alert dialog box
            alert.setTitle(R.string.game_instructions)
            // show alert dialog
            alert.show()
        }

        /*
        val diffArray: Array<Int> = arrayOf(R.string.normal, R.string.difficult)
        diffArray.forEach {
            //println(it)
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }
        */

        // spinner
        var difficulty = "easy"
        binding.difficulty.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                val difficulties = arrayOf("easy", "medium", "hard")
                difficulty = difficulties[position]
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {
                difficulty = "easy"
            }
        }

        // començar joc
        binding.buttonPlay.setOnClickListener {
            val intent = Intent(this, GameActivity::class.java)
            intent.putExtra("difficulty", difficulty)
            startActivity(intent)
        }

        /*
        binding.buttonPlay.setOnClickListener {
            binding.buttonHelp.text = "HOLA"
        }
        */
    }
}